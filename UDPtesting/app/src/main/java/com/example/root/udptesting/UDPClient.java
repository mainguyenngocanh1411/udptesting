package com.example.root.udptesting;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class UDPClient {

    private int port;

    public UDPClient(int port) {
        this.port = port;
    }
    public void setConnection() throws Exception {
        // Nhập chuỗi
        BufferedReader inputStream = new BufferedReader(new InputStreamReader(System.in));
        byte[] sendData = new byte[1024];
        byte[] receiveData = new byte[1024];
        while(true) {

            String sentence = "nana";
            sendData = sentence.getBytes();
            // Tạo socket
            DatagramSocket clientSocket = new DatagramSocket();
            // Tạo gói tin gửi đi thông qua IP address và port bất kì > 1023
            InetAddress IPAddress = InetAddress.getLocalHost();
            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, port);
            clientSocket.send(sendPacket);
            // Nhân gói tin từ server
            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
            clientSocket.receive(receivePacket);
            String modifiedSentence = new String(receivePacket.getData());
            System.out.println("From server: " + modifiedSentence);
            //đóng socket lại
            clientSocket.close();
        }
    }

}