package com.example.root.udptesting;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Button server, client;
    TextView serverTxt, clientTxt;
    Handler handler;
    static final int UdpServerPORT = 4445;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        server = findViewById(R.id.Server);
        client = findViewById(R.id.Client);
        serverTxt = findViewById(R.id.server_text);
        clientTxt = findViewById(R.id.client_text);
        handler = new Handler();
        serverTxt.setText(String.valueOf(UdpServerPORT));
        handler = new Handler();
        Log.d("main", String.valueOf(Thread.currentThread()));
        Thread thread = new Thread() {
            @Override
            public void run() {
                while (true) {
                    Log.d("Run", "run");
                    UDPServer udp = new UDPServer(UdpServerPORT);
                    Log.d("thread", String.valueOf(Thread.currentThread()));
                    try {
                        udp.setConnection();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    handler.post(this);
                }
            }
        };

        thread.start();

        client.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        while (true) {

                            UDPServer udp = new UDPServer(UdpServerPORT);
                            Log.d("thread", String.valueOf(Thread.currentThread()));
                            try {
                                UDPClient client = new UDPClient(64246);
                                try {
                                    client.setConnection();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            handler.post(this);
                        }
                    }
                };
                thread.start();

            }
        });

    }
}
