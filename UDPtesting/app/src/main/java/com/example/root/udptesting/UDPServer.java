package com.example.root.udptesting;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class UDPServer {

    private int port;

    public UDPServer(int port) {
        this.port = port;
    }

    public void setConnection() throws Exception {

        //Tạo socket từ một port bất kì > 2013
        DatagramSocket serverSocket = new DatagramSocket(port);

        while (true) {
            System.out.println("Server is listening...");
            // nhận gói tin từ Client
            byte[] receiveData = new byte[1024];
            DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);

            serverSocket.receive(receivePacket);
            String sentence = new String(receivePacket.getData());

            System.out.println("From client: " + sentence);

            // xử lí: in hoa tất cả các chữ cái
            String capitalizeSentence = sentence.toUpperCase();

            // tìm IP và port của client để gửi về
            InetAddress IPAddress = receivePacket.getAddress();
            int clientPort = receivePacket.getPort();

            //tạo gói tin và gửi Client
            byte[] sendData;
            sendData = capitalizeSentence.getBytes();
            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, clientPort);
            serverSocket.send(sendPacket);
        }

    }

}